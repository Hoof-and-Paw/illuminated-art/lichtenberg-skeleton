import React from 'react';
import ReactDOM from 'react-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faCheckSquare, faCoffee } from '@fortawesome/free-solid-svg-icons';
import { fad, faSkullCrossbones} from '@fortawesome/pro-duotone-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';
library.add(fab, fad, faCheckSquare, faCoffee, faSkullCrossbones);

const features = [
  {
    title: 'Handmade Art!',
    imageUrl: 'img/undraw_docusaurus_mountain.svg',
    description: (
      <>
        \o/
      </>
    ),
  },
  {
    title: (<><i class="fad fa-skull-crossbones"></i>Warning!<i class="fad fa-skull-crossbones"></i></>),
    imageUrl: 'img/lightning.svg',
    description: (
      <>
        <i class="fad fa-skull-crossbones"></i> Lichtenberg Woodburning is <b>DANGEROUS</b>. <i class="fad fa-skull-crossbones"></i><br/>
        <b>Seriously</b>.<br/>
        <span class="fad fa-skull-crossbones"></span> <b>DO NOT</b> try this at home without proper training! <i class="fad fa-skull-crossbones"></i>
      </>
    ),
  },
  {
    title: 'Very Wow!',
    imageUrl: 'img/points.svg',
    description: (
      <>
      Dynamic visualizations allow your piece to mesmerize and captivate you.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Lichtenberg Woodburning Art made for you with love and a bit of insanity by Hoof and Paw">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Get Started
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
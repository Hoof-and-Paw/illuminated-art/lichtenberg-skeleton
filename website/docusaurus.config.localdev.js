/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Skeleton',
  //https://hoof-and-paw.static.wolfspyre.io/illuminated-art/lichtenberg-skeleton
  tagline: 'Starting point for lit art',
  url: 'https://hoof-and-paw.static.wolfspyre.io',
  //baseUrl: '/illuminated-art/lichtenberg-skeleton/',
	// localdev needs an bare baseUrl
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'hoof-and-paw', // Usually your GitHub org/user name.
  projectName: 'lichtenberg-skeleton', // Usually your repo name.
  scripts: [
    {
      src:         'https://kit.fontawesome.com/c2f78a686e.js',
      crossorigin: 'anonymous',
    },
  ],
  // https://docusaurus.io/docs/api/docusaurus-config#stylesheets
  // https://blog.reecemath.com/material-ui-icons-to-docusaurus/
  stylesheets: [
    {
      href: "https://fonts.googleapis.com/icon?family=Material+Icons",
      crossorigin: "anonymous",
      rel: "stylesheet",
    },
  ],
  themeConfig: {
    navbar: {
      title: 'Hoof&Paw',
      logo: {
        alt: 'Hoof&Paw',
        src: 'img/logo.svg',
      },
      items: [
        {
          type: 'dropdown',
          label: 'About your art',
          position: 'left',
          items: [
            {
              to: 'docs/',
              activeBasePath: 'docs',
              label: 'Documentation',
            },
            { 
              to: 'blog', 
              label: 'Creation Notes from your project',
            },
            {
              type: 'doc',
              label: 'Lichtenberg Woodburning',
              docId: 'lichtenberg-woodburning',
            },
            // ... more items
          ],
        },
        {
          href: 'https://www.bhencke.com/pixelblaze',
          label: 'Pixelblaze',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Pixelblaze',
          items: [
            {
              label: 'Pixelblaze for Beginners',
              href: 'https://forum.electromage.com/t/pixelblaze-for-beginners-a-tutorial-syllabus/1679',
            },
            {
              label: 'Pixelblaze',
              href: 'https://www.bhencke.com/mapping-in-pixelblaze/',
            },
            {
              label: 'Pixelblaze Forums',
              href: 'https://forum.electromage.com',
            },
            {
              label: 'Pixelblaze Interview - Embedded.fm',
              href: 'https://embedded.fm/episodes/220',
            },
          ],
        },
        {
          title: 'Hoof and Paw',
          items: [
            {
              label:  ' Instagram',
              id:    'iglink',
              href:  'https://www.instagram.com/hoof.and.paw',
            },
            {
              label: ' Site (CURRENTLY NOWORKY)',
              id:    'hpsitelink',
              href: 'https://hoof-paw.com',
            },
            {
              label: ' Contact Us',
              id:    'hpcontact',
              href: 'mailto:contact@hoof-paw.com',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'GitHub',
              href: 'https://github.com/facebook/docusaurus',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with love.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          //editUrl:
          //  'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          //editUrl:
          //  'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};

---
title: Thank you!
---

## Congratulations on a fine choice in art

By your acquisition of an original Hoof & Paw piece, you've helped bring joy, wonder, and awesome into the world.

Well done!

## What's next?

- some stuff
- even more cool stuff
- did I mention awesomeness?

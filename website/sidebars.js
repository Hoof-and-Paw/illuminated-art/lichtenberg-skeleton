module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Initial Setup',
      items: [
        'thank-you',
        'getting-things-going',
        'lichtenberg-woodburning',
        'markdown-features',
      ],
      type: 'category',
      label: 'troubleshooting',
      items: [
        'troubleshooting',
        'power-requirements',
        'help',
      ],
    },
  ],
};